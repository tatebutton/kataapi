﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using KataAPI.Models;

namespace KataAPI.Services
{
    public class ArabicToRomanService
    {
        private List<RomanNumeral> RomanNumerals { get; set; }

        public ArabicToRomanService()
        {
            RomanNumerals = LoadArabicNumerals();
        }

        public string ConvertArabicToRoman(int num)
        {
            var romanNumeralOutput = "";
            var remainingValue = num;

            while (remainingValue != 0)
            {
                var lowestDenom =
                    RomanNumerals.Where(x => x.IntValue <= Math.Abs(remainingValue))
                        .OrderByDescending(x => x.IntValue)
                        .FirstOrDefault();

                if (lowestDenom.IntValue * (lowestDenom.IntValue * 3) < remainingValue)
                {
                    lowestDenom =
                        RomanNumerals.Where(x => x.IntValue != lowestDenom.IntValue && x.IntValue > lowestDenom.IntValue)
                            .OrderBy(x => x.IntValue)
                            .FirstOrDefault();

                }
                if (remainingValue < 0)
                {
                    remainingValue = remainingValue - (lowestDenom.IntValue * -1);
                    romanNumeralOutput = AddToRomanNumeralOutput(true, romanNumeralOutput, lowestDenom.Symbol);
                }
                else
                {
                    remainingValue = remainingValue - lowestDenom.IntValue;
                    romanNumeralOutput = AddToRomanNumeralOutput(false, romanNumeralOutput, lowestDenom.Symbol);
                }
                
            }

            return romanNumeralOutput;
        }

        private string AddToRomanNumeralOutput(bool left, string romanNumeralOutput, string romanNumeral)
        {
            if (left)
            {
                return $"{romanNumeral}{romanNumeralOutput}";
            }
            else
            {
                return $"{romanNumeralOutput}{romanNumeral}";
            }
        }

        private List<RomanNumeral> LoadArabicNumerals()
        {
            var arabicNumerals = new List<RomanNumeral>();
            arabicNumerals.Add(new RomanNumeralI());
            arabicNumerals.Add(new RomanNumeralV());
            arabicNumerals.Add(new RomanNumeralX());
            arabicNumerals.Add(new RomanNumeralL());
            arabicNumerals.Add(new RomanNumeralC());
            arabicNumerals.Add(new RomanNumeralD());
            arabicNumerals.Add(new RomanNumeralM());
            return arabicNumerals;
        }
    }
}