﻿
using System.Web.Http;
using System.Web.Http.Cors;
using KataAPI.Services;

namespace KataAPI.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [RoutePrefix("api/RomanNumeral")]
    public class RomanNumeralController : ApiController
    {
        
        [Route("ArabicToRoman/{value}")]
        [HttpGet]
        public string ConvertArabicToRoman(int value)
        {
            var svc = new ArabicToRomanService();
            return svc.ConvertArabicToRoman(value);
        }
    }
}