﻿using System.Collections.Generic;
using System.Linq;
using KataAPI.Models;
using KataAPI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KataAPI.Tests.Services
{
    [TestClass]
    public class RomanNumeralServiceTest
    {
        private ArabicToRomanService _arabicToRomanService;


        [TestInitialize]
        public void Init()
        {
            _arabicToRomanService = new ArabicToRomanService();
        }

        
        [TestMethod]
        public void Given1_ShouldReturnI()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(1);
            Assert.AreEqual("I", result);
        }

        [TestMethod]
        public void Given2_ShouldReturnII()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(2);
            Assert.AreEqual("II", result);
        }

        [TestMethod]
        public void Given4_ShouldReturnIV()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(4);
            Assert.AreEqual("IV", result);
        }

        [TestMethod]
        public void Given6_ShouldReturnVI()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(6);
            Assert.AreEqual("VI", result);
        }

        [TestMethod]
        public void Given7_ShouldReturnVII()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(7);
            Assert.AreEqual("VII", result);
        }

        [TestMethod]
        public void Given8_ShouldReturnVIII()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(8);
            Assert.AreEqual("VIII", result);
        }

        [TestMethod]
        public void Given10_ShouldReturnX()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(10);
            Assert.AreEqual("X", result);
        }

        [TestMethod]
        public void Given13_ShouldReturnXIII()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(13);
            Assert.AreEqual("XIII",result);
        }

        

        [TestMethod]
        public void Given15_ShouldReturnXV()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(15);
            Assert.AreEqual("XV",result);
        }

        [TestMethod]
        public void Given1066_ShouldReturnMLXVI()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(1066);
            Assert.AreEqual("MLXVI",result);
        }

        [TestMethod]
        public void Given1989_ShouldReturnMCMLXXXIX()
        {
            var result = _arabicToRomanService.ConvertArabicToRoman(1989);
            Assert.AreEqual("MCMLXXXIX",result);
        }


    }
}
