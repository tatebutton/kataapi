﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KataAPI.Models
{
    public abstract class RomanNumeral 
    {
        public abstract int IntValue { get; set; }
        public abstract string Symbol { get; set; }
        public abstract bool CanBeRepeated { get; set; }
    }
}