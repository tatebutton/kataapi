﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KataAPI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KataAPI.Tests.Models
{
    [TestClass]
   public class RomanNumeralITest
    {
        [TestMethod]
        public void Given1_ShouldReturnI()
        {
            var input = 1;
            var result = new RomanNumeralI().Symbol;
            Assert.AreEqual("I", result);

        }
    }
}
