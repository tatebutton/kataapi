﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KataAPI.Models
{
    public class RomanNumeralD : RomanNumeral
    {
        public sealed override int IntValue { get; set; }
        public sealed override string Symbol { get; set; }
        public sealed override bool CanBeRepeated { get; set; }

        public RomanNumeralD()
        {
            IntValue = 500;
            Symbol = "D";
            CanBeRepeated = false;
        }
    }
}